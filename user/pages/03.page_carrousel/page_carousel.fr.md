---
title: 'Page avec carrousel'
metadata:
    description: 'Page d’exemple avec un carrousel d’entête.'
slug: page-avec-carrousel
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _showcase
            - _what_we_do
            - _portfolio
            - _clients
            - _team
            - _where_we_are
            - _contacts
---

